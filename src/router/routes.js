const routes = [
  {
    path: "/",
    redirect: {
      name: "users-list",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "users-list",
        name: "users-list",
        component: () => import("pages/UsersList.vue"),
      },
      {
        path: "add-user",
        name: "add-user",
        component: () => import("pages/AddUser.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
