import { ref } from "vue";
import { form, submitfrm } from "../pages/AddUser";
import axios from "axios";
import { rows } from "../pages/UsersList";
let rowws = ref([]);
let UsersList = ref([]);
let AddUser = ref([]);
let btnLoadingState = ref(false);
let submit = () => {
  btnLoadingState.value = true;
  axios
    .post("https://jsonplaceholder.typicode.com/users", form.value)
    .then((response) => {
      //   console.log(form.value);
      let idd = rowws.value.length;
      if (response.status === 201) {
        let row = response.data;
        console.log(row);
        rowws.value.push({
          id: ++idd,
          name: row.name,
          username: row.username,
          email: row.email,
          address: {
            street: row.address.street,
            suite: row.address.suite,
            city: row.address.city,
            zipcode: row.address.zipcode,
          },
          phone: row.phone,
          website: row.website,
          company: {
            name: row.company.name,
            catchPhrase: row.company.catchPhrase,
            bs: row.company.bs,
          },
        });

        console.log(rowws.value);
        // KAPAG TINANGGALAN KO ITO NG COMMENT AY NAGLOLOKO PAGNAVIGATE KO SA MGA PAGE rows.value.unshift(rowws.value);
        form.value = {
          name: "",
          username: "",
          email: "",
          address: {
            street: "",
            suite: "",
            city: "",
            zipcode: "",
          },
          phone: "",
          website: "",
          company: {
            name: "",
            catchPhrase: "",
            bs: "",
          },
        };
        submitfrm.value.reset(); // add this line to reset the form
      }
      btnLoadingState.value = false;
    });
};

const getTodos = () => {
  axios.get("https://jsonplaceholder.typicode.com/users").then((response) => {
    rows.value = response.data;
    rows.value = rowws.value;
  });
};

getTodos();

export { UsersList, AddUser, submit, getTodos, btnLoadingState };
